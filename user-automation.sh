cd /home/ansibleuser/ansible-playbooks/pubkeys_devs
sudo git pull
rm -rf /tmp/pubkeys
cd /home/ansibleuser/ansible-playbooks/pubkeys_devs/pubkeys-devs-upload

ls -lt | head -6 > /tmp/pubkeys
awk -F " " '{print $9}' /tmp/pubkeys > /tmp/pubkeys.new
sed -e 's/^/- username: /' /tmp/pubkeys.new > /tmp/pubkeys.newest
sed -i '1d' /tmp/pubkeys.newest
sed '1 s/^/userslist:\n/' /tmp/pubkeys.newest > /tmp/pubkeys.final
cat /tmp/pubkeys.final
cp -rpv /tmp/pubkeys.final /home/ansibleuser/ansible-playbooks/pubkeys_devs/user-vars.yaml

ls -lt  > /tmp/pubkeys_full
awk -F " " '{print $9}' /tmp/pubkeys_full > /tmp/pubkeys_full.new
sed -e 's/^/- username: /' /tmp/pubkeys_full.new > /tmp/pubkeys_full.newest
sed -i '1d' /tmp/pubkeys_full.newest
sed '1 s/^/userslist:\n/' /tmp/pubkeys_full.newest > /tmp/pubkeys_full.final
cat /tmp/pubkeys_full.final
cp -rpv /tmp/pubkeys_full.final /home/ansibleuser/ansible-playbooks/pubkeys_devs/user-vars_full.yaml


cd /home/ansibleuser/ansible-playbooks/pubkeys_devs/
ansible-playbook ssh-access-users.yaml -u ec2-user --private-key=/home/ansibleuser/whjr-v2-stage-key-pair.pem
